import pathlib

class ServiceConfig:
    serverPort = '3333'
    urlBase = 'https://diploma.hom.bry.com.br'
    #urlBase = 'https://diploma.bry.com.br'
    urlInicializacao = urlBase + '/api/xml-signature-service/v2/signatures/initialize'
    urlFinalizacao = urlBase + '/api/xml-signature-service/v2/signatures/finalize'

    token = 'TOKEN_GERADO_NO_BRY_CLOUD'
    returnType = 'LINK'     #Ou BASE64 

    #nome do documento na pasta XMLsOriginais
    documentoDiploma = 'exemplo-xml-diplomado.xml'
    documentoDiplomado = 'exemplo-xml-documentacao.xml'

    caminhoBase = str(pathlib.Path().resolve())
    caminhoDownloads = caminhoBase + '/Downloads/'
    caminhoDiploma = caminhoBase + '/XMLsOriginais/' + documentoDiploma
    caminhoDocumentacao = caminhoBase + '/XMLsOriginais/' + documentoDiplomado