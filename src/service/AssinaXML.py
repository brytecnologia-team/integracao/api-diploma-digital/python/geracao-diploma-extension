import pathlib
import requests
import base64
import sys
import requests
import json
sys.path.append('./src/config/')
sys.path.append('./src/models/')

from ServiceConfig import ServiceConfig
from InitializeResponse import InitializeResponse
from InitializedAndSignedDocuments import InitializedAndSignedDocuments

class AssinaXML:
    def __init__(self):
        self.token = ServiceConfig.token
        self.urlInicializacao = ServiceConfig.urlInicializacao
        self.urlFinalizacao = ServiceConfig.urlFinalizacao

    def InitializeDiploma(self, initializeRequest):
        header = {}
        header['Authorization'] = self.token

        form = {}
        form['nonce'] = 1
        form['signatureFormat'] = 'ENVELOPED'
        form['hashAlgorithm'] = 'SHA256'
        form['certificate'] = initializeRequest.getCertificate()
        form['originalDocuments[0][nonce]'] = '1'
        form['returnType'] = ''

        file = open(initializeRequest.getFilePath(), 'rb')
        files = {
                    'originalDocuments[0][content]' : file
                }

        tipoAssinante = initializeRequest.getTipoAssinante()
        if tipoAssinante == 'Representantes':
            print("Tipo de Assinatura: Pessoas Fisicas (Representantes)")
            form['profile'] = 'ADRC'
            form['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
            #Pode ser necessário alterar este parâmetro para DadosRegistroNSF de acordo com o arquivo sendo utilizado.
            form['originalDocuments[0][specificNode][name]'] = 'DadosRegistro'
        elif tipoAssinante == 'IESRegistradora':
            print("Tipo de Assinatura: IESRegistradora")
            form['profile'] = 'ADRA'
            form['includeXPathEnveloped'] = 'false'
        else:
            print('tipo de assinante não esperado:', tipoAssinante)
            return '{\"message\": \"Erro ao identificar o tipo de assinante em assinatura de Diploma: ' + tipoAssinante + '\"}'
        
        response = requests.request('POST', self.urlInicializacao, headers=header, data = form, files=files)

        if response.status_code != 200:
            print(response.text)
            return '{\"message\": \"Um erro não esperado aconteceu: ' + json.loads(response.text)['message'] + '\"}'

        jsonResponse = response.json()
        nonce = jsonResponse['nonce']
        signedAttributesArray = jsonResponse['signedAttributes']
        initializedDocumentsArray = jsonResponse['initializedDocuments']
        
        initializedDocuments = []
        for x in initializedDocumentsArray:
            nonce = x['nonce']
            content = x['content']
            initializedDocument = InitializedAndSignedDocuments(content, nonce)

            initializedDocuments.append(initializedDocument)

        signedAttributes = []
        for x in signedAttributesArray:
            nonce = x['nonce']
            content = x['content']
            signedAttribute = InitializedAndSignedDocuments(content, nonce)

            signedAttributes.append(signedAttribute)

        initializeResponse = InitializeResponse(initializedDocuments, signedAttributes)

        return initializeResponse.getJson()

    def InitializeDocumentacao(self, initializeRequest):
        header = {}
        header['Authorization'] = self.token

        form = {}
        form['nonce'] = 1
        form['signatureFormat'] = 'ENVELOPED'
        form['hashAlgorithm'] = 'SHA256'
        form['certificate'] = initializeRequest.getCertificate()
        form['originalDocuments[0][nonce]'] = '1'
        form['returnType'] = ''

        file = open(initializeRequest.getFilePath(), 'rb')
        files = {
                    'originalDocuments[0][content]' : file
                }

        tipoAssinante = initializeRequest.getTipoAssinante()
        if tipoAssinante == 'Representantes':
            print("Tipo de Assinatura: Decano ou Reitor (Representantes)")
            form['profile'] = 'ADRC'
            form['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
            #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
            form['originalDocuments[0][specificNode][name]'] = 'DadosDiploma'
        elif tipoAssinante == 'IESEmissoraDadosDiploma':
            print("Tipo de Assinatura: IESEmissora em Dados Diploma")
            form['profile'] = 'ADRC'
            form['originalDocuments[0][specificNode][namespace]'] = 'http://portal.mec.gov.br/diplomadigital/arquivos-em-xsd'
            #Pode ser necessário alterar este parâmetro para DadosDiplomaNSF de acordo com o arquivo sendo utilizado.
            form['originalDocuments[0][specificNode][name]'] = 'DadosDiploma'
            form['includeXPathEnveloped'] = 'false'
        elif tipoAssinante == 'IESEmissoraRegistro':
            print("Tipo de Assinatura: IESEmissora para Registro")
            form['profile'] = 'ADRA'
            form['includeXPathEnveloped'] = 'false'
        else:
            print('tipo de assinante não esperado:', tipoAssinante)
            return '{\"message\": \"Erro ao identificar o tipo de assinante em assinatura de Documentação: ' + tipoAssinante + '\"}'  
        
        response = requests.request('POST', self.urlInicializacao, headers=header, data = form, files=files)

        if response.status_code != 200:
            print(response.text)
            return '{\"message\": \"Um erro não esperado aconteceu: ' + json.loads(response.text)['message'] + '\"}'

        jsonResponse = response.json()
        nonce = jsonResponse['nonce']
        signedAttributesArray = jsonResponse['signedAttributes']
        initializedDocumentsArray = jsonResponse['initializedDocuments']
        
        initializedDocuments = []
        for x in initializedDocumentsArray:
            nonce = x['nonce']
            content = x['content']
            initializedDocument = InitializedAndSignedDocuments(content, nonce)

            initializedDocuments.append(initializedDocument)

        signedAttributes = []
        for x in signedAttributesArray:
            nonce = x['nonce']
            content = x['content']
            signedAttribute = InitializedAndSignedDocuments(content, nonce)

            signedAttributes.append(signedAttribute)

        initializeResponse = InitializeResponse(initializedDocuments, signedAttributes)

        return initializeResponse.getJson()
        
    def Finalize(self, finalizeRequest):
        header = {}
        header['Authorization'] = self.token

        form = {}
        form['nonce'] = 1
        form['signatureFormat'] = 'ENVELOPED'
        form['hashAlgorithm'] = 'SHA256'
        form['certificate'] = finalizeRequest.getCertificate()
        form['returnType'] = finalizeRequest.getReturnType()
        form['originalDocuments[0][nonce]'] = '1'
        form['finalizations[0][signatureValue]'] = finalizeRequest.getCifrado()
        form['finalizations[0][initializedDocument]'] = finalizeRequest.getInitializedDocuments()

        form = self.addProfile(form, finalizeRequest.getTipoAssinante())
        form = self.addXPath(form, finalizeRequest.getTipoAssinante())


        file = open(finalizeRequest.getFilePath(),'rb')
        files = {
                    'finalizations[0][content]' : file   
                }

        response = requests.request('POST', self.urlFinalizacao, headers=header, data=form, files=files)

        if response.status_code != 200:
            print(response.text)
            return '{\"message\": \"Um erro não esperado aconteceu: ' + json.loads(response.text)['message'] + '\"}'

        if finalizeRequest.getReturnType() == 'LINK':
            return response.json()
        else:
            jsonResponse = response.json()
            content = jsonResponse[0]

            decodedContent = base64.b64decode(content)

            full_path = ServiceConfig.caminhoBase + '/XMLsAssinados/documento-assinado.xml'
            diploma = open(full_path, 'w')
            diploma.write(decodedContent.decode("utf-8"))
            diploma.close()
                        
            print('Arquivo salvo localmente em: ' + full_path)
            return '{\"message\": \"Diploma armazenado localmente em: ' + full_path + '\"}'

    def addProfile(self, form, tipoAssinante):
        if tipoAssinante == "IESRegistradora" or tipoAssinante == "IESEmissoraRegistro":
            form['profile'] = "ADRA"
        else:
            form['profile'] = "ADRC"
        
        return form

    def addXPath(self, form, tipoAssinante):
        if tipoAssinante == "IESRegistradora" or tipoAssinante == "IESEmissoraRegistro" or tipoAssinante == "IESEmissoraDadosDiploma":
            form['includeXPathEnveloped'] = "false"
        
        return form