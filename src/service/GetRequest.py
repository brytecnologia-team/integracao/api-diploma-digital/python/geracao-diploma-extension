import sys
from werkzeug.utils import secure_filename

sys.path.append('./src/config/')
sys.path.append('./src/models/')

from ServiceConfig import ServiceConfig
from FinalizeRequest import FinalizeRequest
from CopiaNodoRequest import CopiaNodoRequest
from InitializeRequest import InitializeRequest

def getFinalizeRequest(request, tipoDocumento):
    certificate = request.form['certificate']
    tipoAssinante = request.form['tipoAssinante']
    initializedDocuments = request.form['initializedDocuments']
    cifrado = request.form['cifrado']

    file_path = ""
    returnType = ""
    if request.files:
        returnType = 'LINK'
        file_path = saveFile(request, 'documento')
    else:
        returnType = 'BASE64'
        if tipoDocumento == 'XMLDiplomado':
            file_path = ServiceConfig.caminhoDiploma
        else:
            file_path = ServiceConfig.caminhoDocumentacao

    return FinalizeRequest(certificate, tipoAssinante, initializedDocuments, cifrado, file_path, returnType)

def getInitializeRequest(request, tipoDocumento):
    certificate = request.form['certificate']
    tipoAssinante = request.form['tipoAssinante']

    file_path = ""
    if request.files:
        file_path = saveFile(request, 'documento')
    else:
        if tipoDocumento == 'XMLDiplomado':
            file_path = ServiceConfig.caminhoDiploma
        else:
            file_path = ServiceConfig.caminhoDocumentacao

    return InitializeRequest(certificate, tipoAssinante, file_path)

def  copiaNodoRequestFromRequest(request):
    pathDocumentacaoAcademica = ""
    pathXMLDiplomado = ""
    returnType = ""
    if request.files:
        returnType = 'xml'
        pathDocumentacaoAcademica = saveFile(request, 'documentacaoAcademica')
        pathXMLDiplomado = saveFile(request, 'XMLDiplomado')
    else:
        returnType = 'LOCAL'
        pathDocumentacaoAcademica = ServiceConfig.caminhoDocumentacao
        pathXMLDiplomado = ServiceConfig.caminhoDiploma

    return CopiaNodoRequest(pathDocumentacaoAcademica, pathXMLDiplomado, returnType)

def saveFile(request, fileKey):
        file = request.files[fileKey]
        filename = secure_filename(file.filename)
        file_path = ServiceConfig.caminhoDownloads + filename
        new_file = open(file_path,'wb')
        file.save(new_file)
        new_file.close()

        return file_path
