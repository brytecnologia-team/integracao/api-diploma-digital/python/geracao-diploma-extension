import sys
import xml.dom.minidom

sys.path.append('./src/config/')
sys.path.append('./src/models/')

from ServiceConfig import ServiceConfig

class CopiaNodoXML:
    def copiaNodo(self, request):
        print("Requisição recebida: \n", request)

        DOMTreeDiplomado = xml.dom.minidom.parse(request.getPathXMLDiplomado())
        DOMTreeDocumentacao = xml.dom.minidom.parse(request.getPathDocumentacaoAcademica())

        documentElementDiplomado = DOMTreeDiplomado.documentElement
        documentElementDocumentacao = DOMTreeDocumentacao.documentElement

        dadosDiplomaNodeList = documentElementDocumentacao.getElementsByTagName('DadosDiploma')
        if dadosDiplomaNodeList.length == 0:
            dadosDiplomaNodeList = documentElementDocumentacao.getElementsByTagName('DadosDiplomaNSF')

            if dadosDiplomaNodeList.length == 0:
                return "{\"message\":\"Erro ao recuperar  Nodo DadosDiploma\"}"

        dadosDiplomaNode = dadosDiplomaNodeList.item(0)

        infDiplomaNode = documentElementDiplomado.getElementsByTagName('infDiploma').item(0)
        dadosRegistroNode = documentElementDiplomado.getElementsByTagName('DadosRegistro').item(0)
        infDiplomaNode.insertBefore(dadosDiplomaNode, dadosRegistroNode)

        if request.getReturnType() == "LOCAL":
            full_path = ServiceConfig.caminhoBase + '/XMLsAssinados/exemplo-copia-nodo.xml'
            file = open(full_path,'w')
            file.write(DOMTreeDiplomado.toxml())
            file.close()
            return '{\"message\": \"Arquivo armazenado localmente em: ' + full_path + '\"}'
        else:
            return DOMTreeDiplomado.toxml()