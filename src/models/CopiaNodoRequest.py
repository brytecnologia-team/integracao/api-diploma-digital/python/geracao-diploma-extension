class CopiaNodoRequest:
    def __init__(self, pathDocumentacaoAcademica, pathXMLDiplomado, returnType):
        self.pathDocumentacaoAcademica = pathDocumentacaoAcademica
        self.pathXMLDiplomado = pathXMLDiplomado
        self.returnType = returnType

    def getPathDocumentacaoAcademica(self):
        return self.pathDocumentacaoAcademica

    def getPathXMLDiplomado(self):
        return self.pathXMLDiplomado
    
    def getReturnType(self):
        return self.returnType    

    def __str__(self):
        return "[ \n\tpathDocAcademica: " + self.pathDocumentacaoAcademica + " , \n\tpathDiplomado: " + self.pathXMLDiplomado + " , \n\treturnType: " + self.returnType + " \n]"