import sys

sys.path.append('./src/models/')

from InitializedAndSignedDocuments import InitializedAndSignedDocuments

class InitializeResponse:
    def __init__(self):
        self.nonce = 0
        self.initializedDocuments = []
        self.signedAttributes = []

    def __init__(self, initializedDocuments, signedAttributes):
        self.nonce = 0
        self.initializedDocuments = initializedDocuments
        self.signedAttributes = signedAttributes

    def getNonce(self):
        return self.nonce
    
    def setNonce(self, nonce):
        self.nonce = nonce

    def getInitializedDocuments(self):
        return self.initializedDocuments

    def setInitializedDocuments(self, initializedDocuments):
        self.initializedDocuments = initializedDocuments

    def getSignedAttributes(self):
        return self.signedAttributes

    def setSignedAttributes(self, signedAttributes):
        self.signedAttributes = signedAttributes

    def getJson(self):
        json = {}
        json ['nonce'] = self.nonce

        initializedDocumentsJson = []
        for x in self.initializedDocuments:
            initializedDocumentsJson.append(x.getJson())

        signedAttributesJson = []
        for x in self.signedAttributes:
            signedAttributesJson.append(x.getJson())

        json['initializedDocuments'] = initializedDocumentsJson
        json['signedAttributes'] = signedAttributesJson

        return json