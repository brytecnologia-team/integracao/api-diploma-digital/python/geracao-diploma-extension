class InitializedAndSignedDocuments:
    def __init__(self):
        self.content = ''
        self.nonce = 0

    def __init__(self, content, nonce):
        self.content = content
        self.nonce = nonce

    def getContent(self):
        return self.content

    def setContent(self, content):
        self.content = content

    def getNonce(self):
        return self.nonce

    def setNonce(self, nonce):
        self.nonce = nonce

    def getJson(self):
        json = {}
        json['content'] = self.content
        json['nonce'] = self.nonce

        return json