class FinalizeRequest:
    def __init__(self):
        self.certificate = ''
        self.tipoAssinante = ''
        self.initializedDocuments = ''
        self.cifrado = ''
        self.filePath = ''
        self.returnType = ''

    def __init__(self, certificate, tipoAssinante, initializedDocuments, cifrado, filePath, returnType):
        self.certificate = certificate
        self.tipoAssinante = tipoAssinante
        self.initializedDocuments = initializedDocuments
        self.cifrado = cifrado
        self.filePath = filePath
        self.returnType = returnType

    def getCertificate(self):
        return self.certificate

    def setCertificate(self, certificate):
        self.certificate = certificate

    def getTipoAssinante(self):
        return self.tipoAssinante

    def setTipoAssinante(self, tipoAssinante):
        self.tipoAssinante = tipoAssinante

    def getInitializedDocuments(self):
        return self.initializedDocuments

    def setInitializedDocuments(self, initializedDocuments):
        self.initializedDocuments = initializedDocuments

    def getCifrado(self):
        return self.cifrado

    def setCifrado(self, cifrado):
        self.cifrado = cifrado

    def getFilePath(self):
        return self.filePath

    def setFilePath(self, filePath):
        self.filePath = filePath
    
    def getReturnType(self):
        return self.returnType

    def setReturnType(self, returnType):
        self.returnType = returnType