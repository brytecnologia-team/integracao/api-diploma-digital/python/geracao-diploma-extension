class InitializeRequest:
    def __init__(self, certificate, tipoAssinante, filePath):
        self.certificate = certificate
        self.tipoAssinante = tipoAssinante
        self.filePath = filePath
    
    def getCertificate(self):
        return self.certificate
        
    def setCertificate(self, certificate):
        self.certificate = certificate

    def getTipoAssinante(self):
        return self.tipoAssinante

    def setTipoAssinante(self, tipoAssinante):
        self.tipoAssinante = tipoAssinante

    def getFilePath(self):
        return self.filePath

    def setFilePath(self, filePath):
        self.filePath = filePath