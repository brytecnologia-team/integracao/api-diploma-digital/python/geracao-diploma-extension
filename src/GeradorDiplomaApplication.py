import sys
import json
from flask_cors import CORS
from flask import Flask, request

from models.InitializeResponse import InitializeResponse

sys.path.append('./src/config/')
sys.path.append('./src/service/')

from AssinaXML import AssinaXML
from CopiaNodoXML import CopiaNodoXML
from ServiceConfig import ServiceConfig
from GetRequest import getFinalizeRequest, getInitializeRequest, copiaNodoRequestFromRequest



app = Flask(__name__)
CORS(app)

@app.route('/**', methods=['OPTIONS'])
def optionsInicializaPublico():
    return '{\'Allow\':\"POST\", \"GET\", \"OPTIONS\", \"DELETE\", \"PATCH\", \"PUT"\"}'

@app.route('/XMLDiplomado/inicializa', methods=['POST'])
def postInicializaPublico():
    print('Method: Post', ', Endpoint: /XMLDiplomado/inicializa')
    initializeRequest = getInitializeRequest(request, 'XMLDiplomado')

    response = AssinaXML().InitializeDiploma(initializeRequest)

    return response

@app.route('/XMLDiplomado/finaliza', methods=['POST'])
def postFinalizaPublico():
    print('Method: Post', ', Endpoint: /XMLDiplomado/finaliza')
    finalizeRequest = getFinalizeRequest(request, 'XMLDiplomado')
    
    response = AssinaXML().Finalize(finalizeRequest)
    return response

@app.route('/XMLDocumentacaoAcademica/inicializa', methods=['POST'])
def postInicializaPrivado():
    print('Method: Post', ', Endpoint: /XMLDocumentacaoAcademica/inicializa')
    initializeRequest = getInitializeRequest(request, 'XMLDocumentacaoAcademica')

    response = AssinaXML().InitializeDocumentacao(initializeRequest)
    return response
@app.route('/XMLDocumentacaoAcademica/finaliza', methods=['POST'])
def postFinalizaPrivado():
    print('Method: Post', ', Endpoint: /XMLDocumentacaoAcademica/finaliza')
    finalizeRequest = getFinalizeRequest(request, 'XMLDocumentacaoAcademica')
    
    response = AssinaXML().Finalize(finalizeRequest)
    return response

@app.route('/XMLDiplomado/copia-nodo', methods=['POST'])
def teste():
    print('Method: Post', ', Endpoint: /XMLDiplomado/copia-nodo')
    copiaNodoRequest = copiaNodoRequestFromRequest(request)

    response = CopiaNodoXML().copiaNodo(copiaNodoRequest)

    return response

if __name__ == '__main__':
    app.run(host="localhost", port=ServiceConfig.serverPort, debug=True)